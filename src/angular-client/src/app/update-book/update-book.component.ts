import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormRecord, Validators } from '@angular/forms';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { Book } from '../models/book';
import { ServiceBookService } from '../services/service-book.service';

@Component({
  selector: 'app-update-book',
  templateUrl: './update-book.component.html',
  styleUrls: ['./update-book.component.css']
})
export class UpdateBookComponent implements OnInit, OnDestroy {


  private id=0;
  public bookForm!: FormGroup;
  private book:Book={

     id:0,
     title:"",
     author:"",
     genre:"",
     year:0
  }

  constructor(private route:ActivatedRoute, private service: ServiceBookService, private secondRoute: Router){}

  ngOnDestroy(): void {

    
  }

  ngOnInit(): void {

   this.createUpdateForm();
  
    this.route.params.subscribe(params=>{
      this.id = params['id'];
      console.log(this.id);

   

      this.service.findBookByID(this.id).subscribe(data=>{
        this.book = data;
        this.createUpdateForm();
  
      });


     })
  

  }

  private createUpdateForm(){

    this.bookForm = new FormGroup({

      id: new FormControl(this.book.id,[
        Validators.min(1),
        Validators.required
      ]),

      title: new FormControl(this.book.title,[
        Validators.min(1),
        Validators.required
      ]),

      author: new FormControl(this.book.author,[
        Validators.min(1),
        Validators.required
      ]),

      genre: new FormControl(this.book.genre,[
        Validators.min(1),
        Validators.required
      ]),

      year: new FormControl(this.book.year,[
        Validators.min(1),
        Validators.required
      ])
    },
    {
      updateOn:'change'
    })

  }


  public deleteTheBook(){


    console.log("aici");
    this.service.deleteBook(this.id).subscribe((data)=>{
      //manevrare eraore

          this.secondRoute.navigate(['/books']);

      
    })

  }

  public updateTheBook(){

    if(this.bookForm.valid){

      this.service.updateBook(this.bookForm.value).subscribe(data=>{

        this.secondRoute.navigate(['/books']);

      })
    }

  }

 

}
