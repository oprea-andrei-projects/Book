import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ServiceBookService } from '../services/service-book.service';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit, OnDestroy {
 

  public bookForm!: FormGroup;
  constructor(private bookService:ServiceBookService, private route: Router){

  }


  ngOnDestroy(): void {
    
  }
  ngOnInit(): void {

    this.createForm();
    
  }

  private createForm(){

    this.bookForm = new FormGroup({
      title: new FormControl(null,[
        Validators.min(1),
        Validators.required,
      ]),
      author: new FormControl(null,[
        Validators.min(1),
        Validators.required,
      ]),
      genre: new FormControl(null,[
        Validators.min(1),
        Validators.required,
      ]),
      year: new FormControl(null,[
        Validators.min(1),
        Validators.required,
      ]),
    },
    {
      updateOn:'change'
    })
  }

  public addBook(){


    if(this.bookForm.valid){

       this.bookService.addBook(this.bookForm.value).subscribe(
        {
          next: ()=>{
              this.route.navigate(['/books']);
          },
          error:  (error)=>{
             console.log(error)
          },
          complete: ()=>{
             console.log("complete");
          }
        }
       )
    }

    
  }

}


