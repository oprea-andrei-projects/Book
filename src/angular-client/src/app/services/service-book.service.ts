import { EnvironmentInjector, Injectable } from '@angular/core';
import { BehaviorSubject, catchError, Observable, of, tap } from 'rxjs';
import { Book } from '../models/book';

import { environment } from "../../environments/environment";
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceBookService {

  private server = environment.apiUrl + "/api/v1/books";

  public booksChanged = new BehaviorSubject<Book[]>([]);

  constructor(private http: HttpClient) {
    this.getBooks().subscribe(response => {
      this.booksChanged.next(response);
    })
  }

  getBooks(): Observable<Array<Book>> {
    return this.http.get<Array<Book>>(this.server + "/allBooks").pipe(
      catchError(this.handleError)
    );
  }

  addBook(book: Book): Observable<Book> {
    let b=book;
    book.id = this.generateBookId([...this.booksChanged.value]);
    this.booksChanged.next([...this.booksChanged.value, book]);
    return this.http.post<Book>(this.server + "/addBook", b);
  }

  deleteBook(id: number): Observable<Book> {
    console.log("+++++++" + id);
    this.booksChanged.next([...this.booksChanged.value.filter(e=>e.id!=id)]);
    return this.http.delete<Book>(this.server + "/deleteBook/" + id);
  }

  findBookByID(id: number): Observable<Book> {
    return this.http.get<Book>(this.server + "/findBook/" + id);
  }

  updateBook(book: Book): Observable<Book> {

    this.booksChanged.next([...this.booksChanged.value.filter(e=>e.id!=book.id), book]);

    return this.http.put<Book>(this.server + "/updateBook", book);
  }


  private generateBookId(books: Book[]): number {



    let ids = books.map(book => book.id);


    let id = Math.floor(Math.random() * 10000);
    while (ids.includes(id) == true) {

      id = Math.floor(Math.random() * 10000);
    }


    return id;


  }





  private handleError(error: HttpErrorResponse): Observable<any> {

    console.log(error);

    let errorMessage: string;
    if (error.error instanceof ErrorEvent) {
      errorMessage = `Client Error - ${error.error.message}`;

    } else {

      if (error.error.reason) {
        errorMessage = `${error.error.reason} - Error code ${error.status}`;
      } else {
        errorMessage = `An error occurred - Error code ${error.status}`;
      }
    }
    return of(errorMessage);
  }
}
