import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddBookComponent } from './add-book/add-book.component';
import { HomeComponent } from './home/home.component';
import { UpdateBookComponent } from './update-book/update-book.component';

const routes: Routes = [

{path:'', redirectTo:'/books',pathMatch:'full'},
{path:'books',component:HomeComponent},
{path:'books/new', component:AddBookComponent},
{path:'books/:id', component:UpdateBookComponent}



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
