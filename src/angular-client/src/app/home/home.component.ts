import { Component, OnDestroy, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Book } from '../models/book';
import { ServiceBookService } from '../services/service-book.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  public books!:Book[];

  public subscription= new Subscription();

  constructor(private serviceBookService: ServiceBookService, private router:Router) {
  }

  public ngOnDestroy(): void {

    this.subscription.unsubscribe();
   
  }
  public ngOnInit(): void {
   this.subscription= this.serviceBookService.booksChanged.subscribe(data => {
      this.books = data;
    })
  }
 

  test(){
    console.log("nu inveti nimic doar te plangi toata ziua!")
    this.router.navigate(['/books/new']);
  }
}
